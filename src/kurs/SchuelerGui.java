package kurs;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.*;
import javax.swing.*;
import kurs.*;

/**
 *
 */
public class SchuelerGui extends JFrame {
    //Felder
    private JTextField inputName = new JTextField();
    private JTextField inputGeschlecht = new JTextField();
    private JTextField inputDNote = new JTextField();
    private JLabel Schueler = new JLabel();
    private JTextField outputName = new JTextField();
    private JTextField outputGeschlecht = new JTextField();
    private JTextField outputDNote = new JTextField();
    private JTextField selectId = new JTextField();
    private int nextId = 1;

    //AusgabeListe
    JTextArea outputListe = new JTextArea(5, 25);

    //Buttons
    private JButton bEinfuegen = new JButton();
    private JButton bAuswaehlen = new JButton();
    private JButton bLoeschen = new JButton();
    //Schuelerliste
    private List<Schueler> schuelerListe;

    /**
     * Schueler Gui bauen
     */
    public SchuelerGui() {
        // Frame-Initialisierung
        super();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        int frameWidth = 447;
        int frameHeight = 391;
        setSize(frameWidth, frameHeight);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (d.width - getSize().width) / 2;
        int y = (d.height - getSize().height) / 2;
        setLocation(x, y);
        setTitle("Gui");
        setResizable(false);
        Container cp = getContentPane();
        cp.setLayout(null);

        //Eingabefelder
        inputName.setBounds(64, 64, 73, 25);
        cp.add(inputName);
        inputGeschlecht.setBounds(152, 64, 81, 25);
        cp.add(inputGeschlecht);
        inputDNote.setBounds(248, 64, 57, 25);
        inputDNote.setText("");
        cp.add(inputDNote);
        Schueler.setBounds(120, 8, 81, 25);
        Schueler.setText("Schueler");
        cp.add(Schueler);

        outputListe.setBounds(64, 130, 300, 120);
        cp.add(outputListe);

        //Ausgabefelder
        outputName.setBounds(64, 316, 73, 25);
        outputGeschlecht.setBounds(152, 316, 81, 25);
        outputDNote.setBounds(248, 316, 57, 25);
        cp.add(outputName);
        cp.add(outputGeschlecht);
        outputDNote.setText("");
        cp.add(outputDNote);

        selectId.setBounds(64, 264, 25, 25);
        cp.add(selectId);

        //Buttons
        bEinfuegen.setBounds(120, 104, 81, 17);
        bEinfuegen.setText("einfuegen");
        bEinfuegen.setMargin(new Insets(2, 2, 2, 2));
        bEinfuegen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                bEinfuegen_ActionPerformed(evt);
            }
        });
        cp.add(bEinfuegen);

        bAuswaehlen.setBounds(120, 264, 57, 17);
        bAuswaehlen.setText("Anzeigen");
        bAuswaehlen.setMargin(new Insets(2, 2, 2, 2));
        bAuswaehlen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                bAuswaehlen_ActionPerformed(evt);
            }
        });
        cp.add(bAuswaehlen);

        bLoeschen.setBounds(192, 264, 65, 17);
        bLoeschen.setText("Löschen");
        bLoeschen.setMargin(new Insets(2, 2, 2, 2));
        bLoeschen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                bLoeschen_ActionPerformed(evt);
            }
        });
        cp.add(bLoeschen);

        //Leere Schuelerliste
        schuelerListe = new List();

        setVisible(true);
    } // end of public Gui

    //Programmstart
    public static void main(String[] args) {
        SchuelerGui schuelerGui = new SchuelerGui();
        schuelerGui.initData();
        schuelerGui.drawSchuelerListe();
    }
    
    public void initData() {
        Schueler neuerSchueler;
        neuerSchueler = new Schueler("Jochen1", "M", 1.5);
        neuerSchueler.setId(nextId++);
        schuelerListe.append(neuerSchueler);
        
        Schueler neuerSchueler1;
        neuerSchueler1 = new Schueler("Uschi", "W", 2.5);
        neuerSchueler1.setId(nextId++);
        schuelerListe.append(neuerSchueler1);
        
        Schueler neuerSchueler2;
        neuerSchueler2 = new Schueler("Günther", "M", 1.7);
        neuerSchueler2.setId(nextId++);
        schuelerListe.append(neuerSchueler2);
    }

    /**
     * Schueler hinzufügen
     * @param evt 
     */
    public void bEinfuegen_ActionPerformed(ActionEvent evt) {
        //Neuen Schüler erzeugen
        Schueler neuerSchueler;
        neuerSchueler = new Schueler();
        neuerSchueler.setId(nextId);
        neuerSchueler.setName(inputName.getText());
        neuerSchueler.setGeschlecht(inputGeschlecht.getText());
        neuerSchueler.setdNote(Double.parseDouble(inputDNote.getText()));
        System.out.println("neuer Schueler: " + neuerSchueler.toString());
        //zur Liste hinzufügen
        schuelerListe.append(neuerSchueler);

        //System.out.println("Anzahl: " + schuelerListe.size());
        System.out.println("Schüler: " + neuerSchueler.toString());

        
        //Zur Anzeige hinzufügen
        //outputListe.append("ID: " + neuerSchueler.getId() + " | Name: " + neuerSchueler.getName() + "\n");
        this.drawSchuelerListe();

        //Formular löschen
        this.deleteInputFields();
        //ID Feld um 1 erhöhen
        nextId++;
    }

    /**
     * Schüler anzeigen
     * @param evt 
     */
    public void bAuswaehlen_ActionPerformed(ActionEvent evt) {
        int selectedId = Integer.parseInt(selectId.getText());
        this.showSchueler(selectedId);
    }

    /**
     * Schüeler löschen
     * @param evt 
     */
    public void bLoeschen_ActionPerformed(ActionEvent evt) {
        int selectedId = Integer.parseInt(selectId.getText());
        this.removeFromSchuelerList(selectedId);
    }

    private void deleteInputFields() {
        inputName.setText("");
        inputGeschlecht.setText("");
        inputDNote.setText("");
    }
    
    private void deleteOutputFields() {
        outputName.setText("");
        outputGeschlecht.setText("");
        outputDNote.setText("");
    }

    /**
     * Per ID ausgewählten Schüler aus der Liste entfernen
     *
     * @param selectedId
     */
    private void removeFromSchuelerList(int selectedId) {
        schuelerListe.toFirst();
        Boolean weiter = true;
        while(weiter) {
            kurs.Schueler content = schuelerListe.getContent();
            if(null != content) {
                System.out.println(content.toString());
                if(content.getId()==selectedId) {
                    schuelerListe.remove();
                    weiter = false;
                }
            }
            else {
                weiter = false;
            }
            schuelerListe.next();
        }
        drawSchuelerListe();
        deleteOutputFields();
    }

    /**
     * Ausgabe der Schuelerliste
     */
    private void drawSchuelerListe() {
        outputListe.setText("");
        schuelerListe.toFirst();
        Boolean weiter = true;
        while(weiter) {         
            kurs.Schueler content = schuelerListe.getContent();
            if(null!= content) {
                outputListe.append("ID: [" + content.getId() + "] Name: " + content.getName() + "\n");
            }
            else {
                weiter = false;                
            }
            schuelerListe.next();
        }
    }

    /**
     * Anzeige eines gewählten Schülers
     */
    private void showSchueler(int selectedId) {
        this.deleteOutputFields();
        schuelerListe.toFirst();
        Boolean weiter = true;
        while(weiter) {         
            kurs.Schueler schueler = schuelerListe.getContent();
            if(null!= schueler) {
                if(schueler.getId()==selectedId) {
                    outputName.setText(schueler.getName());
                    outputGeschlecht.setText(schueler.getGeschlecht());
                    outputDNote.setText(schueler.getdNote().toString());
                    weiter = false;
                }
            }
            else {
                weiter = false;                
            }
            schuelerListe.next();
        }

    }

}


/**
 * Schüler Klasse
 */
class Schueler {
    private int id;
    private String name;
    private String geschlecht;
    private Double dNote;

    public Schueler() {
    }
       
    public Schueler(String name, String geschlecht, Double dNote) {
        this.name = name;
        this.geschlecht = geschlecht;
        this.dNote = dNote;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeschlecht() {
        return geschlecht;
    }

    public void setGeschlecht(String geschlecht) {
        this.geschlecht = geschlecht;
    }

    public Double getdNote() {
        return dNote;
    }

    public void setdNote(Double dNote) {
        this.dNote = dNote;
    }

    @Override
    public String toString() {
        return "Schueler{" + "id=" + id + ", name=" + name + ", geschlecht=" + geschlecht + ", dNote=" + dNote + '}';
    }
    
    
    
}

